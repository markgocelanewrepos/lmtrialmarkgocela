/**
 * Angular App: lmtrial
 */
(function() {
  'use strict';

  angular.module('lmtrial', [
      'ngCookies',
      'ui.router',
      'ngTable',
      'ncy-angular-breadcrumb',
  ]).run(Main);

  function Main($rootScope, $state, $stateParams, UserSession) {
    UserSession.Check();
    $rootScope.$state = $state;

    console.log($rootScope.$state);
  }
})();