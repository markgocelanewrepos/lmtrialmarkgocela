/**
 * Angulalar Factory: Auth
 */
(function() {
  'use strict';

  angular.module('lmtrial')
         .factory('Auth', Auth);

  function Auth($http, $q, $rootScope, API_PATH, AUTH) {
    var service = {
      Login: Login,
      Logout: Logout
    }

    return service;

    function Login(credentials) {
      var d = $q.defer();

      $http({
        method: 'POST',
        url: API_PATH.URL + AUTH.LOGIN,
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(credentials)
      })
      .success(function(data) {
        d.resolve(data);
      })
      .error(function(data, status) {
        d.reject({
          data: data,
          status: status
        });
      });

      return d.promise;
    }

    function Logout() {
      var d = $q.defer();

      $http({
        method: 'POST',
        url: API_PATH.URL + AUTH.LOGIN,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + $rootScope.token
        }
      })
      .success(function(data) {
        d.resolve(data);
      })
      .error(function(status) {
        d.reject(status);
      });

      return d.promise;
    }
  }
})();