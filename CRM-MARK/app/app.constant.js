/*
 * Angular Dependencies: lmtrial.constants
 */
(function(){
  'use strict';
  angular.module('lmtrial')
    .constant('APP_PATH', {
      BASE_URL: '/contacts',
      LOGIN_URL: '/login',
      INFOS: '/infos'
    })
    .constant('NG_PATH', {
      CONTROLLERS: 'app/controllers/',
      DIRECTIVES: 'app/directives/',
      FACTORIES: 'app/factories/',
      SERVICES: 'app/services/'
    })
    .constant('API_PATH', {
      URL: 'https://testapi.nzfsg.co.nz/' /*API*/
    })
    .constant('AUTH', {
      LOGIN: 'login'
    })
    .constant('CONTACT', {
      GET_ALL_FAMILY: 'contacts/FamilyListGet'
    })
    .constant('SUMMARY', {
      GET_INFO_SUMMARY: 'contacts/ContactFamilyInfoGet'
    })
    .constant('INFO', {
      GET_INFO_ONE: 'contacts/ClientInformGet'
    })
    .constant('COOKIE', {
      AUTH_TOKEN: 'auth_token'
    });
})();
