/**
 * Angular Controller: LoginController
 *
 * Login Controller
 */
(function() {
  'use strict';

  angular.module('lmtrial')
         .controller('LoginController', LoginController);

  function LoginController($rootScope, $scope, Auth, UserSession) {

     $scope.Login = function(email,password){
                      if (email== null && password == null) {
                                     $.dialog({
                                        icon: 'fa fa-exclamation-triangle',
                                        title: 'Required!',
                                        content: 'username or password'
                                  });
                                     return false;
                     }

                     var aletDialog =  $.dialog({
                          closeIcon: false,
                          icon: 'fa fa-spinner fa-spin',
                          title: 'Logging in!',
                          content: 'Logging in account!'
                    });


                             if (email && password) {
                                      UserSession.SetEmailPass(email,password);
                                      Auth.Login({
                                        'username' : email,
                                        'password' : password
                                      }).then(function (response){
                                        UserSession.Set(response);
                                            aletDialog.close();
                                      }, function (error) {
                                        console.log(error);
                                                aletDialog.close();
                                                $.alert("Message:  "+error.data+"<br>Status:(Unauthorized)  "+error.status);
                                      });
                                    } 
    }

  }
    
})();