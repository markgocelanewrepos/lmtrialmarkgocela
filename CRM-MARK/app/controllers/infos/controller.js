/**
 * Contacts Controller
 */
(function() {
  'use strict';

  angular.module('lmtrial')
         .controller('InfoController', InfoController);

  function InfoController($rootScope, $scope, $state, $stateParams, infoSummaryService,infoService) {
   var FamilyID = $stateParams.id;

    function _init() {
    $scope.letters = [];
    $scope.infoSummary;
    $scope.infoOne;
      infoSummaryService.RetrieveListFamilyDataInfoSummary(FamilyID)
        .then(function(response){
         	$scope.infoSummary =response;
         	console.log("controller",$scope.infoSummary);
         
        });

         infoService.RetrieveDataInfoClient(FamilyID)
             .then(function(response){
          $scope.infoOne =response;
          console.log("controllerOne",$scope.infoOne);
         
        });

    }

    _init();

  }
})();