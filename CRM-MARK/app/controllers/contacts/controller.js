/**
 * Contacts Controller
 */
(function() {
  'use strict';

  angular.module('lmtrial')
         .controller('ContactController', ContactController);

  function ContactController($rootScope, $scope, $state, $stateParams, ContactService) {
    var vm = this;

    vm.contact_count = 0;
    vm.isChecked = false;
    vm.family_lists = [];

    function _init() {
      var tmp = [];
    $scope.letters = [];

      ContactService.RetrieveListFamilyData()
        .then(function(response){
          vm.contact_count = response.TotalNumber;

          angular.forEach(response.FamilyList, function(value, key) {
              $rootScope.FamilyName = value.FamilyFullname;

              tmp.push(value);
              console.log($stateParams);
              console.log("tmp",tmp);

            vm.family_lists = tmp;

            setTimeout(function() {
              $('.contacts-main table tbody tr:first-child td').each(function(id) {
                $('.contacts-main table th').eq(id).width($(this).width() + 'px');
              });

              window.perfectScrollbarHandler();
            }, 1000);
          });
        });

        $scope.$watch($stateParams.id, function () {
            $('.breadcrumb li:first-child a').prepend('<i class="fa fa-phone-square"></i>')
        });
        
        for (var i = 65; i <= 90; i++) {
            $scope.letters.push(String.fromCharCode(i));
        }
        $scope.ShowAddModalContact = function(){
          alert("currently in progress.");
        }
    }

    _init();

  }
})();