/**
 * Contact Service: Contact
 * @author MARK GOCELA <alecogkram@gmail.com>
 */
 (function() {
  'use strict';

  angular.module('lmtrial')
         .service('ContactService', ContactService);

  function ContactService($http, $q, $rootScope, UserSession, API_PATH, CONTACT) {
    var service = {
      RetrieveListFamilyData: RetrieveListFamilyData
    };

    return service;

    function RetrieveListFamilyData() {
      var d = $q.defer();


      var alertDialog =  $.dialog({
                        closeIcon: false,
                          icon: 'fa fa-spinner fa-spin',
                          title: 'Fetching all contacts!',
                          content: 'Please wait..'
                    });

      $http({
        method: 'GET',
        url: API_PATH.URL + CONTACT.GET_ALL_FAMILY + '?startWith=*&byPassFilter=true',
        headers: {
          'Authorization': 'Bearer ' + $rootScope.token
        }
      })
      .success(function(data) {
        d.resolve(data);
        console.log("data",data);
        alertDialog.close();
      })
      .error(function(response) {
        if(response == "Your Session has expired") {
          UserSession.Logout();
        }
        d.reject(response);
      });

      return d.promise;
    }
  }
 })();