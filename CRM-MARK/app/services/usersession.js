/**
 * User Session Service: UserSession
 */
(function() {
  'use strict';

  angular.module('lmtrial')
         .service('UserSession', UserSession);

  function UserSession($http, $q, $location, HttpService, Auth,
                       $rootScope, $timeout, $cookieStore, API_PATH,
                       COOKIE, APP_PATH) {
    var vm = this;
    vm.IsSet = IsSet;
    vm.Set = Set;
    vm.Check = Check;
    vm.SetEmailPass = SetEmailPass;
    vm.Logout = Logout;

    function IsSet() {
      if($rootScope.email && $rootScope.password) {
        Auth.Login({
          'username': $rootScope.email,
          'password': $rootScope.password
        })
        .then(function(response) {
          console.log(response);
          return true;
        }, function(error) {
          console.log(error);
          return false;
        });
      }

      return false;
    }

    function Set(token) {
      $rootScope.token = token;
      $cookieStore.put(COOKIE.AUTH_TOKEN, token); /*store cookie*/
      console.log(token);
      $timeout(function() {
        $location.path(APP_PATH.BASE_URL);
      }, 800);
    }

    function SetEmailPass(email, password) {
      $rootScope.email = email;
      $rootScope.password = password;
    }

    function Logout() {
      $cookieStore.remove(COOKIE.AUTH_TOKEN);
      HttpService.RemoveAuthHeaders();

      delete $rootScope.token;

      $timeout(function() {
        $location.path(APP_PATH.LOGIN_URL);
      }, 800);
    }

    function Check() {
      if(typeof $cookieStore.get(COOKIE.AUTH_TOKEN) !== 'undefined') {
        $rootScope.token = $cookieStore.get(COOKIE.AUTH_TOKEN);
        HttpService.SetAuthorizationHeader($rootScope.token);
      }

      var redirect = ($rootScope.token) ? APP_PATH.BASE_URL : APP_PATH.LOGIN_URL;

      if(redirect === APP_PATH.BASE_URL && $location.path().length > 2)
        return;

      $timeout(function() {
        $location.path(redirect);
      }, 10);
    }
  }
})();