/**
 * Contact Service: Contact
 * @author MARK GOCELA <alecogkram@gmail.com>
 */
 (function() {
  'use strict';

  angular.module('lmtrial')
         .service('infoService', infoService);

  function infoService($http, $q, $rootScope, UserSession, API_PATH,INFO) {
    var service = {
      RetrieveDataInfoClient: RetrieveDataInfoClient
    };

    return service;

    function RetrieveDataInfoClient(parameterID) {
      var d = $q.defer();

      var uRL = API_PATH.URL + INFO.GET_INFO_ONE+'?familyId='+parameterID;
 
      $http({
        method: 'GET',
        url: uRL,
        headers: {
          'Authorization': 'Bearer ' + $rootScope.token
        }
      })
      .success(function(data) {
        d.resolve(data);
        console.log("dataOne:",data);
      })
      .error(function(response) {
        if(response == "Your Session has expired") {
          UserSession.Logout();
        }
        d.reject(response);
      });

      return d.promise;
    }
  }
 })();