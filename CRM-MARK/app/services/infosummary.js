/**
 * Contact Service: Contact
 * @author MARK GOCELA <alecogkram@gmail.com>
 */
 (function() {
  'use strict';

  angular.module('lmtrial')
         .service('infoSummaryService', infoSummaryService);

  function infoSummaryService($http, $q, $rootScope, UserSession, API_PATH, SUMMARY) {
    var service = {
      RetrieveListFamilyDataInfoSummary: RetrieveListFamilyDataInfoSummary
    };

    return service;

    function RetrieveListFamilyDataInfoSummary(parameterID) {
      var d = $q.defer();


      var alertDialog =  $.dialog({
                        closeIcon: false,
                          icon: 'fa fa-spinner fa-spin',
                          title: 'Fetching summary!',
                          content: 'Please wait ...'
                    });

      var uRL = API_PATH.URL + SUMMARY.GET_INFO_SUMMARY+'?familyId='+parameterID;
 
      $http({
        method: 'GET',
        url: uRL,
        headers: {
          'Authorization': 'Bearer ' + $rootScope.token
        }
      })
      .success(function(data) {
        d.resolve(data);
        console.log("data",data);
        alertDialog.close();
      })
      .error(function(response) {
        if(response == "Your Session has expired") {
          UserSession.Logout();
        }
        d.reject(response);
      });

      return d.promise;
    }
  }
 })();