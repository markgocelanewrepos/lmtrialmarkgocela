/*
 * Angular Configuration: lmtrial.config
 *@property {const} NG_PATH
 */
 (function(){
  'use strict';

  angular.module('lmtrial')
         .config(AppConfig);

  function AppConfig($stateProvider, $urlRouterProvider, $locationProvider, NG_PATH, $breadcrumbProvider) {

    $breadcrumbProvider.setOptions({
      templateLast: '<i ng-bind-html="ncyBreadcrumbLabel"></i>'
    });

    /* Default Routes */
    $urlRouterProvider
        .when('', '/')
        .when('/', '/')
        .otherwise('/:hide');

    /**
     * Application Routes
     *
     * List of routes of the application
     */
    $stateProvider

    // Login Route
    .state({
      name: 'login',
      url: '/login',
      templateUrl: NG_PATH.CONTROLLERS + 'login/view.html',
      controller: 'LoginController',
      resolve: {
        Session: _CheckSession
      },
      ncyBreadcrumb: {
        skip: true
      }
    })
    .state({
      name: 'contacts',
      url: '/contacts',
      abstract: true,
      controller: '',
      templateUrl: NG_PATH.CONTROLLERS + 'contacts/template.html',
      resolve: {
          Session: _CheckSession
      },
      ncyBreadcrumb: {
          skip: true
      }
    })
    .state({
      name: 'contacts.index',
      url: '',
      templateUrl: NG_PATH.CONTROLLERS + 'contacts/view.html',
      controller: 'ContactController as vm', /*vm is Optional alias*/
      ncyBreadcrumb: {
          label: 'Contacts'
      }
    })
    .state({
      name: 'infos',
       url: '/infos/:id',
      templateUrl: NG_PATH.CONTROLLERS + 'infos/view.html',
      controller: 'InfoController', /*vm is Optional alias*/
      ncyBreadcrumb: {
          label: 'Summary'
      }
    });

    function _CheckSession($timeout, $location, UserSession) {
      if(UserSession.IsSet()) {
        var redirect = ($rootScope.token) ? APP_PATH.BASE_URL : APP_PATH.LOGIN_URL;

        if(redirect === APP_PATH.BASE_URL && $location.path().length > 2)
          return;

        $timeout(function() {
          $location.path(redirect);
        }, 10);
      }
    }

  }
 })();